
export class CommentModel {
    public commentId: number;
    public nom: string;
    public dateComment: Date;
    public courriel: string;
    public commentaire: string;
    public twitter: string;
    public visible: boolean;

    public static fromJSON(jsonCommentModel: CommentModel) {
        const commentModel = new CommentModel;
        Object.assign(commentModel, jsonCommentModel);
        commentModel.dateComment = new Date(commentModel.dateComment);
        return commentModel;
    }
}
