
export class RecetteModel {
    public recetteId: number;
    public titre: string;
    public description: string;

    public static fromJSON(jsonRecetteModel: RecetteModel) {
        const recetteModel = new RecetteModel;
        Object.assign(recetteModel, jsonRecetteModel);
        return recetteModel;
    }
}
