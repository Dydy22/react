export enum Role {
    admin = 'Admin',
    gestionCommentaire = 'Cacher/afficher Commentaire',
    modifDescription = 'Modifier la descrition',
    gestionUtilisateur = 'Gestion des utilisateurs'
}

export const roleList = [Role.admin, Role.gestionCommentaire, Role.modifDescription, Role.gestionUtilisateur];
