import { Role } from './role';

export enum Permission {
    gestionComment = 'gestionComment',
    modifyDesc = 'modifyDesc',
    manageUser = 'manageUser',
}

export const rolePermissions = {
    [Role.admin]: [
        Permission.gestionComment,
        Permission.modifyDesc,
        Permission.manageUser],

    [Role.gestionCommentaire]: [
        Permission.gestionComment],

    [Role.modifDescription]: [
        Permission.modifyDesc],

    [Role.gestionUtilisateur]: [
        Permission.manageUser]
};
