import { Role, UserModel } from 'common';
import { DBProvider } from '../dbprovider';

export class AuthDAO {
    private knex = DBProvider.getKnexConnection();

    public async getUser(username: string) {
        const user: UserModel = await this.knex('user').first('userId', 'username', 'password',).where({ username });
        if (!user) { return user; }
        await this.hydrate(user);
        return user;
    }

    public async getUsers() {
        const users: UserModel[] = await this.knex('user').first('userId', 'username');
        await Promise.all(users.map(this.hydrate));
        return users;
    }

    public async getUserById(userId: number) {
        const user: UserModel = await this.knex('user').first('userId', 'username', 'password',).where({ userId });
        if (!user) { return user; }
        await this.hydrate(user);
        return user;
    }

    public async createUser(user: UserModel) {
        const { username, password, roles } = user ;
        try {
            const userId: number = await this.knex('user').insert( {username, password} );
            if ( roles.length > 0 ) {
                await this.knex('role').insert(roles.map(role => { return { userId, role}; }))
            }
            return userId;
        } catch (e) {
            if (e.code !== 'ER_DUP_ENTRY') {
                console.log('Erreur lors de la création de l\'utilisateur', e);
            }
            return null;
        }
    }

    public async updateUser(user: UserModel) {
        const { username, password, userId, roles } = user ;

        if (password) {
            await this.knex('user').update({ username, password }).where({ userId });
        } else {
            await this.knex('user').update({ username }).where({ userId });
        }

        await this.knex('role').delete().where({ userId });

        if (roles.length) {
            await this.knex('role').insert(roles.map( role => { return { role, userId }; }));
        }
    }

    // Ça ajout les roles pour chaque utilisateur.
    private hydrate = async (user: UserModel) => {
        const userId = user.userId;
        const roles: Role[] = await this.knex('role').pluck('role').where({ userId });
        user.roles = roles;
        return user;
    };
}
