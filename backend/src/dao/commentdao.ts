import { CommentModel } from 'common';
import { DBProvider } from '../dbprovider';

export class CommentDAO {
    private knex = DBProvider.getKnexConnection();

    public async createComment(comment: CommentModel) {
        const { nom, courriel, commentaire, twitter } = comment;
        const [commentId] = await this.knex('comment').insert({
            nom, courriel, commentaire, twitter
        });
        return commentId;
    }

    public async getComment(commentId: number | string) {
        const comment = await this.knex('comment').first('*').where({ commentId });
        if (!comment) { return null; }
        return CommentModel.fromJSON(comment);
    }

    public async updateComment(comment: CommentModel) {
        const { commentId: commentId, nom, commentaire, visible } = comment;
        await this.knex('comment').update({ nom, commentaire, visible }).where({ commentId });
    }

    public async deleteComment(commentId: number) {
        await this.knex('comment').delete().where({ commentId });
    }

    public async getComments() {
        const comments = await this.knex('comment').select('*').orderBy('commentId', 'desc'); //on fait select au lieu de first car on les veut tous

        return comments.map(CommentModel.fromJSON);
    }
}
