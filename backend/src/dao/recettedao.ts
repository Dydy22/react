import { RecetteModel } from 'common';
import { DBProvider } from '../dbprovider';

export class RecetteDAO {
    private knex = DBProvider.getKnexConnection();

    public async getRecette(recetteId: number | string) {

        const comment = await this.knex('recette').first('*').where({ recetteId });
        if (!comment) { return null; }
        return RecetteModel.fromJSON(comment);
    }

    public async updateRecette(recette: RecetteModel) {
        const { recetteId: recetteId, titre, description } = recette;
        await this.knex('recette').update({ titre, description }).where({ recetteId });
    }

    public async getRecettes() {
        const recettes = await this.knex('recette').select('*'); //on fait select au lieu de first car on les veut tous

        return recettes.map(RecetteModel.fromJSON);
    }
}
