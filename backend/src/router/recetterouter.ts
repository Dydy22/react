import { Permission, RecetteModel } from 'common';
import { Router } from 'express';
import { RecetteDAO } from '../dao/recettedao';
import { hasPermission, wrap } from '../util';

const recetteRouter = Router();
const recetteDAO = new RecetteDAO;

recetteRouter.use('/:recetteId', wrap(async (req, res, next) => {
    const recette = await recetteDAO.getRecette(req.params.recetteId);
    //si null ca retourne le livre
    if (recette === null) { return res.sendStatus(404); }
    req.recette = recette;

    return next();
}));

// On retourne la liste au complet
recetteRouter.get('/', wrap(async (_req, res) => {
    const recettes = await recetteDAO.getRecettes();
    return res.send(recettes);
}));

recetteRouter.put('/:recetteId', hasPermission(Permission.modifyDesc), wrap(async (req, res) => {
    const updated: RecetteModel = req.body;
    updated.recetteId = req.recette.recetteId; //identifiant comme il est en ce moment

    await recetteDAO.updateRecette(updated); //on met a jour le livre avec le model

    return res.send(await recetteDAO.getRecette(req.recette.recetteId));
}));

export { recetteRouter };

