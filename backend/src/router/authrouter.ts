import bcrypt from 'bcrypt';
import { Permission } from 'common';
import { Router } from 'express';
import passport from 'passport';
import { AuthDAO } from '../dao/authdao';
import { hasPermission, wrap } from '../util';

const authRouter = Router();
const authDAO = new AuthDAO;

authRouter.post('/login',
passport.authenticate('local', { session: true }),
 (req, res) => {
     if(req.user) {
         res.send();
     } else {
         res.sendStatus(401);
     }
 });

authRouter.post('/logout', wrap(async (req, res) => {
    if (!req.session) { return res.send(); }
    req.session.destroy(err => {
        if (err !== undefined) {
            console.error(`Error destroying session, ${err}`);
        }
    });
    return res.send();
}));

authRouter.post('/user', hasPermission(Permission.manageUser), wrap(async (req, res) => {
    const user = req.body;
    user.password = await bcrypt.hash(user.password, 10);

    const createdUserId = await authDAO.createUser(user);
    if (createdUserId === null) { return res.sendStatus(400); }

    const createdUser = (await authDAO.getUserById(createdUserId));
    delete createdUser.password;
    return res.send( createdUser );
}));

authRouter.get('/user', wrap(async (req, res) => {
    if (!req.user) { return res.sendStatus(403); }
    const users = await authDAO.getUsers();
    return res.send(users);
}));

authRouter.get('/user/current', wrap(async (req, res) => {
    if (!req.user) { return res.sendStatus(404); }
    return res.send(req.user);
}));

const loginHandler = async (username: string, password: string, done: (error: any, user?: any) => void) => {
    const user = await authDAO.getUser(username);

    if (user === null) {
        return done(null, false);
    }
    if (await bcrypt.compare(password, user.password!)) {
        delete user.password;
        return done(null, user);
    }
    return done(null, false);
};
 export { authRouter, loginHandler };
