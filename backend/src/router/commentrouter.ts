import { CommentModel } from 'common';
import {  Router } from 'express';
import { CommentDAO } from '../dao/commentdao';
import { wrap } from '../util';
import { recetteRouter } from './recetterouter';

const commentRouter = Router();
const commentDAO = new CommentDAO;

commentRouter.use('/:commentId', wrap(async (req, res, next) => {
    const comment = await commentDAO.getComment(req.params.commentId);
    if (comment === null) { return res.sendStatus(404); }
    req.comment = comment;

    return next();
}));

commentRouter.get('/', wrap(async (_req, res) => {
    const comments = await commentDAO.getComments();
    return res.send(comments);
}));

commentRouter.get('/:commentId', wrap(async (req, res) => {
    return res.send(req.comment);
}));

commentRouter.post('/', wrap(async (req, res) => {
    const comment = CommentModel.fromJSON(req.body);
    const commentId = await commentDAO.createComment(comment);
    return res.send(await commentDAO.getComment(commentId));
}));

commentRouter.use('/:commentId/recette', recetteRouter);

export { commentRouter };


