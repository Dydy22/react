import { CommentModel, RecetteModel, UserModel } from "common";

declare global {
    module Express {
        interface Request {
            comment: CommentModel;
            recette: RecetteModel;
            // userModel: UserModel;
        }
        interface User extends UserModel { }
    }
}
