import { UserContextComponent } from 'context/usercontext';
import { MainRouter } from 'mainrouter';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<UserContextComponent> <MainRouter /> </UserContextComponent>, document.getElementById('coreContainer'));
// ReactDOM.render( <MainRouter />, document.getElementById('coreContainer'));
