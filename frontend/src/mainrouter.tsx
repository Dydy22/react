import { CommentView } from 'component/commentview';
import { LoginForm } from 'component/loginform';
import { LogoutButton } from 'component/logoutbutton';
import { UserManager } from 'component/usermanager';
import { BASE_HREF } from 'config.json';
import { LivreDor } from 'livredor';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

interface Props { }
interface State { }

export class MainRouter extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    public render() {

        return <BrowserRouter basename={BASE_HREF}>
            <div className='container'>
                {this.context.user && <LogoutButton />}
                <Switch>
                    <Route path='/login' component={LoginForm} />
                    <Route path='/:commentId' component={CommentView} />
                    <Route path='/usermanager' component={UserManager} />
                    <Route path='/' component={LivreDor} />
                </Switch>
            </div>
            <footer id='footer' />

        </BrowserRouter>;
    }
};
