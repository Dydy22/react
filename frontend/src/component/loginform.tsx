import { Api } from 'api';
import { UserModel } from 'common';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';

interface Props { }
interface State { username?: string; password?: string; }

export class LoginForm extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        if (this.context.user === undefined) { return null;}
        if (this.context.user === undefined) {
            return <Redirect to='/' />;
        }

        return <div>
            <Link to='/register' >S'enregistrer</Link>
            <form onSubmit={this.login}>
                <label htmlFor='utilisateur'>Utilisateur :</label>
                <input type='text' id='utilisateur' required={true} value={this.state.username ?? ''} onChange={ e => {
                    this.setState({ username: e.target.value });
                }}/>

                <label htmlFor='motDePasse'>Mot de passe :</label>
                <input type='password' id='motDePasse' required={true} value={this.state.password ?? ''} onChange={ e => {
                    this.setState({ password: e.target.value });
                }}/>
                <input type='submit' />
            </form>
        </div>;
    }

    private login = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            const user = UserModel.fromJSON(await this.api.postGetJson('/auth/login', { username: this.state.username, password: this.state.password }));
            this.context.setUser(user);
        } catch {
            alert('Indentification incorrecte.');
        }
    }
}
