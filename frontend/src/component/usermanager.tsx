import { Api } from 'api';
import { Permission, roleList, UserModel } from 'common';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Redirect } from 'react-router';

interface Props { }
interface State { users: UserModel[]; user: UserModel; }

export class UserManager extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = { users: [], user: new UserModel };
    }

    public async componentDidMount() {
        const users = (await this.api.getJson('/auth/user') as any[]).map(UserModel.fromJSON);
            this.setState({ users })
    }

    public render() {
        const { users, user } = this.state;
        if (!this.context.user?.hasPermission(Permission.manageUser)) {
            return <Redirect to='/' />;
        }

        return <>
            <div>
                <div style={{ width: '20%', display: 'inline-block'}}>
                    <select className='form-control from-control-4' size={30} onChange={( e: React.ChangeEvent<HTMLSelectElement>) => {
                        const userToEdit = users.find(u => u.userId === parseInt(e.currentTarget.value));
                        this.setState({ user: userToEdit ?? new UserModel });
                        }}>
                            <option style={{ fontWeight: 'bold'}}>Nouveau</option>
                            {users.map(u => <option value={u.userId} key={u.userId}>{u.username}</option>)}
                    </select>
                </div>
                <div style={{ width: '80%', display: 'inline-block'}}>
                    <form onSubmit={this.saveOrCreate}>
                        <div>
                            <label htmlFor='utilisateur'>Nom d'utilisateur :</label>
                            <input type='text' id='utilisateur' value={user?.username ?? ''} onChange={ e => {
                                user.username = e.currentTarget.value;
                                this.setState({ user });
                                }}/>
                        </div>
                        <div>
                            <label htmlFor='password'>Mot de passe :</label>
                            <input type='password' id='password' value={user?.password ?? ''} onChange={ e => {
                                user.password = e.currentTarget.value;
                                this.setState({ user });
                                }}/>
                        </div>
                        <div>
                            {roleList.map(role => <> <label key={role} htmlFor='utilisateur'>{role}</label>
                                <input type='checkbox' checked={user.roles.includes(role) ?? false}
                                    onChange={
                                        () => {
                                            if (user.roles.includes(role)) {
                                                user.roles = user.roles.filter(currentRole => currentRole !== role);
                                            } else {
                                                user.roles.push(role);
                                            }
                                            this.setState({ user });
                                        }
                                    } />
                            </>)};
                        </div>
                        <button type='submit'>{user.userId ? 'Sauvegarder': 'Créer'}</button>
                    </form>
                </div>
            </div>
        </>;
    }

    private saveOrCreate = async (e: React.FormEvent) => {
        e.preventDefault();
        const user = this.state.user!;
        let newUser: UserModel | undefined = undefined;

        if (user.userId) {
            newUser = UserModel.fromJSON(await this.api.putGetJson('/auth/user', user.userId, user));
        } else {
            newUser = UserModel.fromJSON(await this.api.postGetJson('/auth/user', user));
        }

        const users = (await this.api.getJson('/auth/user')).map(UserModel.fromJSON);
        this.setState({ users, user: newUser });
    }
}
