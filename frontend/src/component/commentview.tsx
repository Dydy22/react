import { Api } from 'api';
import { CommentModel } from 'common';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

interface Props extends RouteComponentProps<{ commentId: string; }> { }
interface State { comment?: CommentModel; }

export class CommentView extends React.Component<Props, State> {
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public async componentDidMount() {
        const commentId = this.props.match.params.commentId;

        const comment = CommentModel.fromJSON(await this.api.getJson(`/comment/${commentId}`));
        this.setState({ comment });

    }

    public render() {
        const { comment } = this.state;
        if (!comment) { return 'Chargement...'; }
        // if (activity === undefined) { return 'Chargement...'; };
        // if (activity === null) { return 'Cette activité n\'existe pas'; }

        const dateFormat = { year: 'numeric', month: 'long', day: 'numeric' };
        return <>
            <nav>
                <Link to='/'>Page d'acceuil</Link>
            </nav>
            <div style={{ border: '1px solid' }}
                key={comment.commentId}>
                <h2> Nom : {comment.nom}</h2>
                <h6> Date : {comment.dateComment.toLocaleDateString(undefined, dateFormat)}</h6>
                 <p> Commentaire : {comment.commentaire}</p>
            </div>
        </>;
    }
}
