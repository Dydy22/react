
import { Api } from 'api';
import { CommentModel, Permission, RecetteModel } from 'common';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Link } from 'react-router-dom';



interface Props { }
interface State {
    comments?: CommentModel[];
    recettes?: RecetteModel[];
    nom?: string;
    twitter?: string,
    courriel?: string,
    commentaire?: string,
    description?: string,
    recetteId: number;
}

export class LivreDor extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;
    private api = new Api;

    constructor(props: Props) {
        super(props);
        this.state = {
            recetteId: 1
        };
    }

    public async componentDidMount() {
        const comments = (await this.api.getJson('/comment') as any[]).map(CommentModel.fromJSON);
        const recettes = (await this.api.getJson('/recette') as any[]).map(RecetteModel.fromJSON);
        this.setState({ comments });
        this.setState({ recettes });
    }

    public render() {
        const { comments } = this.state;
        const { recettes } = this.state;
        const { user } = this.context;

        // if (user === undefined) { return 'Chargement...'; }
        if (!comments) { return 'Chargement...'; }
        if (!recettes) { return 'Chargement...'; }


        const dateFormat = { year: 'numeric', month: 'long', day: 'numeric' };
        return <>

            {recettes.map(recette => <div key={recette.recetteId}>
                <h1>{recette.titre}</h1>
                <img src='./img/gateau.jpg' id='gateau' />

                { (user?.hasPermission(Permission.gestionComment)) &&
                    <form onSubmit={this.updateRecette}>
                        <div>
                            <textarea value={this.state.description ?? recette.description} required={true} onChange={e => this.setState({ description: e.target.value })} />
                        </div>
                        <button type='submit'>Soumettre changement</button>
                    </form>
                }

                <p>{recette.description}</p>

            </div>)}

            <hr />
            <hr />
            <hr />

            <form onSubmit={this.createComment} >
                <label>Nom :
                    <input type='text' value={this.state.nom ?? ''} onChange={e => this.setState({ nom: e.target.value })} />
                </label>
                <label>Pseudo Twitter :
                    <input type='text' value={this.state.twitter ?? ''} onChange={e => this.setState({ twitter: e.target.value })} />
                </label>
                <label>Courriel :
                    <input type='email' value={this.state.courriel ?? ''} onChange={e => this.setState({ courriel: e.target.value })} />
                </label>
                <label>Commentaire :
                    <textarea required={true} value={this.state.commentaire ?? ''} onChange={e => this.setState({ commentaire: e.target.value })} />
                </label>
                <button>Envoyer</button>
            </form>

            <hr />
            <hr />

            {comments.map(comment => <div key={comment.commentId}>

                <Link to={`${comment.commentId}`}> <button>Visionner</button> </Link>
           { user?.hasPermission(Permission.gestionComment) ? <button onClick={() => this.hideComment(comment)}>{comment.visible ? 'Cacher' : 'Afficher'}</button> : null}
                <div style={{ opacity: comment.visible ? '100%' : '20%' }}>
                    <h2> {comment.nom}</h2>
                    <h6> Date : {comment.dateComment.toLocaleDateString(undefined, dateFormat)}</h6>
                    <p> Pseudo Twitter : {comment.twitter} </p>
                    <p> Courriel : {comment.courriel}</p>
                    <p> Commentaire : </p>
                    <p> {comment.commentaire}</p>
                    <hr />
                </div>
            </div>
            )}

        </>;
    }

    private createComment = async (e: React.FormEvent) => {
        e.preventDefault();
        const comment = {
            nom: (this.state.nom === '' || this.state.nom === undefined) ? 'Anonyme' : this.state.nom,
            twitter: this.state.twitter,
            courriel: this.state.courriel,
            commentaire: this.state.commentaire
        };

        const createcomment = CommentModel.fromJSON(await this.api.postGetJson('/comment', comment));
        this.state.comments!.unshift(createcomment);

        this.setState({ comments: this.state.comments, nom: '', twitter: '', courriel: '', commentaire: '' });
    };

    private hideComment = async (commentToUpdate: CommentModel) => {
        const { comments } = this.state;

        if (commentToUpdate.visible === true) {
            commentToUpdate.visible = false;
            await this.api.put('/comment', commentToUpdate.commentId, commentToUpdate);
            this.setState({ comments });
        } else {
            commentToUpdate.visible = true;
            await this.api.put('/comment', commentToUpdate.commentId, commentToUpdate);
            this.setState({ comments });
        }
    };

    private updateRecette = async (e: React.FormEvent) => {
        e.preventDefault();
        const { recettes } = this.state;
        if (recettes === undefined) { return; }

        const recette = recettes.find(r => r.recetteId === this.state.recetteId)!;
        recette.description = this.state.description!;

        await this.api.put('/recette', recette.recetteId, recette);
        this.setState({ recettes });
    };

}
